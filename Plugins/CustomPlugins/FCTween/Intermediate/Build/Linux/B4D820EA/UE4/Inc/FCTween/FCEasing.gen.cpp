// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FCTween/Public/FCEasing.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFCEasing() {}
// Cross Module References
	FCTWEEN_API UEnum* Z_Construct_UEnum_FCTween_EFCEase();
	UPackage* Z_Construct_UPackage__Script_FCTween();
// End Cross Module References
	static UEnum* EFCEase_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_FCTween_EFCEase, Z_Construct_UPackage__Script_FCTween(), TEXT("EFCEase"));
		}
		return Singleton;
	}
	template<> FCTWEEN_API UEnum* StaticEnum<EFCEase>()
	{
		return EFCEase_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFCEase(EFCEase_StaticEnum, TEXT("/Script/FCTween"), TEXT("EFCEase"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_FCTween_EFCEase_Hash() { return 1087866743U; }
	UEnum* Z_Construct_UEnum_FCTween_EFCEase()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_FCTween();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFCEase"), 0, Get_Z_Construct_UEnum_FCTween_EFCEase_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFCEase::Linear", (int64)EFCEase::Linear },
				{ "EFCEase::Smoothstep", (int64)EFCEase::Smoothstep },
				{ "EFCEase::Stepped", (int64)EFCEase::Stepped },
				{ "EFCEase::InSine", (int64)EFCEase::InSine },
				{ "EFCEase::OutSine", (int64)EFCEase::OutSine },
				{ "EFCEase::InOutSine", (int64)EFCEase::InOutSine },
				{ "EFCEase::InQuad", (int64)EFCEase::InQuad },
				{ "EFCEase::OutQuad", (int64)EFCEase::OutQuad },
				{ "EFCEase::InOutQuad", (int64)EFCEase::InOutQuad },
				{ "EFCEase::InCubic", (int64)EFCEase::InCubic },
				{ "EFCEase::OutCubic", (int64)EFCEase::OutCubic },
				{ "EFCEase::InOutCubic", (int64)EFCEase::InOutCubic },
				{ "EFCEase::InQuart", (int64)EFCEase::InQuart },
				{ "EFCEase::OutQuart", (int64)EFCEase::OutQuart },
				{ "EFCEase::InOutQuart", (int64)EFCEase::InOutQuart },
				{ "EFCEase::InQuint", (int64)EFCEase::InQuint },
				{ "EFCEase::OutQuint", (int64)EFCEase::OutQuint },
				{ "EFCEase::InOutQuint", (int64)EFCEase::InOutQuint },
				{ "EFCEase::InExpo", (int64)EFCEase::InExpo },
				{ "EFCEase::OutExpo", (int64)EFCEase::OutExpo },
				{ "EFCEase::InOutExpo", (int64)EFCEase::InOutExpo },
				{ "EFCEase::InCirc", (int64)EFCEase::InCirc },
				{ "EFCEase::OutCirc", (int64)EFCEase::OutCirc },
				{ "EFCEase::InOutCirc", (int64)EFCEase::InOutCirc },
				{ "EFCEase::InElastic", (int64)EFCEase::InElastic },
				{ "EFCEase::OutElastic", (int64)EFCEase::OutElastic },
				{ "EFCEase::InOutElastic", (int64)EFCEase::InOutElastic },
				{ "EFCEase::InBounce", (int64)EFCEase::InBounce },
				{ "EFCEase::OutBounce", (int64)EFCEase::OutBounce },
				{ "EFCEase::InOutBounce", (int64)EFCEase::InOutBounce },
				{ "EFCEase::InBack", (int64)EFCEase::InBack },
				{ "EFCEase::OutBack", (int64)EFCEase::OutBack },
				{ "EFCEase::InOutBack", (int64)EFCEase::InOutBack },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "InBack.Comment", "// anticipation; pull back a little before going forward\n" },
				{ "InBack.Name", "EFCEase::InBack" },
				{ "InBack.ToolTip", "anticipation; pull back a little before going forward" },
				{ "InBounce.Name", "EFCEase::InBounce" },
				{ "InCirc.Name", "EFCEase::InCirc" },
				{ "InCubic.Name", "EFCEase::InCubic" },
				{ "InElastic.Name", "EFCEase::InElastic" },
				{ "InExpo.Comment", "// like Quintic but even sharper\n" },
				{ "InExpo.Name", "EFCEase::InExpo" },
				{ "InExpo.ToolTip", "like Quintic but even sharper" },
				{ "InOutBack.Name", "EFCEase::InOutBack" },
				{ "InOutBounce.Name", "EFCEase::InOutBounce" },
				{ "InOutCirc.Name", "EFCEase::InOutCirc" },
				{ "InOutCubic.Name", "EFCEase::InOutCubic" },
				{ "InOutElastic.Name", "EFCEase::InOutElastic" },
				{ "InOutExpo.Name", "EFCEase::InOutExpo" },
				{ "InOutQuad.Name", "EFCEase::InOutQuad" },
				{ "InOutQuart.Name", "EFCEase::InOutQuart" },
				{ "InOutQuint.Name", "EFCEase::InOutQuint" },
				{ "InOutSine.Name", "EFCEase::InOutSine" },
				{ "InQuad.Name", "EFCEase::InQuad" },
				{ "InQuart.Name", "EFCEase::InQuart" },
				{ "InQuint.Name", "EFCEase::InQuint" },
				{ "InSine.Name", "EFCEase::InSine" },
				{ "Linear.Name", "EFCEase::Linear" },
				{ "ModuleRelativePath", "Public/FCEasing.h" },
				{ "OutBack.Name", "EFCEase::OutBack" },
				{ "OutBounce.Name", "EFCEase::OutBounce" },
				{ "OutCirc.Name", "EFCEase::OutCirc" },
				{ "OutCubic.Name", "EFCEase::OutCubic" },
				{ "OutElastic.Name", "EFCEase::OutElastic" },
				{ "OutExpo.Name", "EFCEase::OutExpo" },
				{ "OutQuad.Name", "EFCEase::OutQuad" },
				{ "OutQuart.Name", "EFCEase::OutQuart" },
				{ "OutQuint.Name", "EFCEase::OutQuint" },
				{ "OutSine.Name", "EFCEase::OutSine" },
				{ "Smoothstep.Name", "EFCEase::Smoothstep" },
				{ "Stepped.Name", "EFCEase::Stepped" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_FCTween,
				nullptr,
				"EFCEase",
				"EFCEase",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
