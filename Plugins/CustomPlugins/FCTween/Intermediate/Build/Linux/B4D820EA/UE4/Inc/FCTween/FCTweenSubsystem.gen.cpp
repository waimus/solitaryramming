// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FCTween/Public/FCTweenSubsystem.h"
#include "Engine/Classes/Engine/GameInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFCTweenSubsystem() {}
// Cross Module References
	FCTWEEN_API UClass* Z_Construct_UClass_UFCTweenSubsystem_NoRegister();
	FCTWEEN_API UClass* Z_Construct_UClass_UFCTweenSubsystem();
	ENGINE_API UClass* Z_Construct_UClass_UGameInstanceSubsystem();
	UPackage* Z_Construct_UPackage__Script_FCTween();
// End Cross Module References
	void UFCTweenSubsystem::StaticRegisterNativesUFCTweenSubsystem()
	{
	}
	UClass* Z_Construct_UClass_UFCTweenSubsystem_NoRegister()
	{
		return UFCTweenSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UFCTweenSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastTickedFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_LastTickedFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastRealTimeSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LastRealTimeSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFCTweenSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstanceSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_FCTween,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFCTweenSubsystem_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FCTweenSubsystem.h" },
		{ "ModuleRelativePath", "Public/FCTweenSubsystem.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFCTweenSubsystem_Statics::NewProp_LastTickedFrame_MetaData[] = {
		{ "ModuleRelativePath", "Public/FCTweenSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UClass_UFCTweenSubsystem_Statics::NewProp_LastTickedFrame = { "LastTickedFrame", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFCTweenSubsystem, LastTickedFrame), METADATA_PARAMS(Z_Construct_UClass_UFCTweenSubsystem_Statics::NewProp_LastTickedFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFCTweenSubsystem_Statics::NewProp_LastTickedFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFCTweenSubsystem_Statics::NewProp_LastRealTimeSeconds_MetaData[] = {
		{ "ModuleRelativePath", "Public/FCTweenSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFCTweenSubsystem_Statics::NewProp_LastRealTimeSeconds = { "LastRealTimeSeconds", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFCTweenSubsystem, LastRealTimeSeconds), METADATA_PARAMS(Z_Construct_UClass_UFCTweenSubsystem_Statics::NewProp_LastRealTimeSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFCTweenSubsystem_Statics::NewProp_LastRealTimeSeconds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFCTweenSubsystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFCTweenSubsystem_Statics::NewProp_LastTickedFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFCTweenSubsystem_Statics::NewProp_LastRealTimeSeconds,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFCTweenSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFCTweenSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFCTweenSubsystem_Statics::ClassParams = {
		&UFCTweenSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFCTweenSubsystem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFCTweenSubsystem_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFCTweenSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFCTweenSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFCTweenSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFCTweenSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFCTweenSubsystem, 3065809317);
	template<> FCTWEEN_API UClass* StaticClass<UFCTweenSubsystem>()
	{
		return UFCTweenSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFCTweenSubsystem(Z_Construct_UClass_UFCTweenSubsystem, &UFCTweenSubsystem::StaticClass, TEXT("/Script/FCTween"), TEXT("UFCTweenSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFCTweenSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
