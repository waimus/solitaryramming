// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FCTWEEN_FCTweenBPAction_generated_h
#error "FCTweenBPAction.generated.h already included, missing '#pragma once' in FCTweenBPAction.h"
#endif
#define FCTWEEN_FCTweenBPAction_generated_h

#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_8_DELEGATE \
static inline void FTweenEventOutputPin_DelegateWrapper(const FMulticastScriptDelegate& TweenEventOutputPin) \
{ \
	TweenEventOutputPin.ProcessMulticastDelegate<UObject>(NULL); \
}


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_SPARSE_DATA
#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetTimeMultiplier); \
	DECLARE_FUNCTION(execStop); \
	DECLARE_FUNCTION(execRestart); \
	DECLARE_FUNCTION(execUnpause); \
	DECLARE_FUNCTION(execPause);


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetTimeMultiplier); \
	DECLARE_FUNCTION(execStop); \
	DECLARE_FUNCTION(execRestart); \
	DECLARE_FUNCTION(execUnpause); \
	DECLARE_FUNCTION(execPause);


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFCTweenBPAction(); \
	friend struct Z_Construct_UClass_UFCTweenBPAction_Statics; \
public: \
	DECLARE_CLASS(UFCTweenBPAction, UBlueprintAsyncActionBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FCTween"), NO_API) \
	DECLARE_SERIALIZER(UFCTweenBPAction)


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUFCTweenBPAction(); \
	friend struct Z_Construct_UClass_UFCTweenBPAction_Statics; \
public: \
	DECLARE_CLASS(UFCTweenBPAction, UBlueprintAsyncActionBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FCTween"), NO_API) \
	DECLARE_SERIALIZER(UFCTweenBPAction)


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFCTweenBPAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFCTweenBPAction) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFCTweenBPAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFCTweenBPAction); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFCTweenBPAction(UFCTweenBPAction&&); \
	NO_API UFCTweenBPAction(const UFCTweenBPAction&); \
public:


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFCTweenBPAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFCTweenBPAction(UFCTweenBPAction&&); \
	NO_API UFCTweenBPAction(const UFCTweenBPAction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFCTweenBPAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFCTweenBPAction); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFCTweenBPAction)


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_PRIVATE_PROPERTY_OFFSET
#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_10_PROLOG
#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_PRIVATE_PROPERTY_OFFSET \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_SPARSE_DATA \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_RPC_WRAPPERS \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_INCLASS \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_PRIVATE_PROPERTY_OFFSET \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_SPARSE_DATA \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_INCLASS_NO_PURE_DECLS \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FCTWEEN_API UClass* StaticClass<class UFCTweenBPAction>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
