// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FCTween/Public/FCTweenInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFCTweenInstance() {}
// Cross Module References
	FCTWEEN_API UEnum* Z_Construct_UEnum_FCTween_EDelayState();
	UPackage* Z_Construct_UPackage__Script_FCTween();
// End Cross Module References
	static UEnum* EDelayState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_FCTween_EDelayState, Z_Construct_UPackage__Script_FCTween(), TEXT("EDelayState"));
		}
		return Singleton;
	}
	template<> FCTWEEN_API UEnum* StaticEnum<EDelayState>()
	{
		return EDelayState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDelayState(EDelayState_StaticEnum, TEXT("/Script/FCTween"), TEXT("EDelayState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_FCTween_EDelayState_Hash() { return 751795492U; }
	UEnum* Z_Construct_UEnum_FCTween_EDelayState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_FCTween();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDelayState"), 0, Get_Z_Construct_UEnum_FCTween_EDelayState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDelayState::None", (int64)EDelayState::None },
				{ "EDelayState::Start", (int64)EDelayState::Start },
				{ "EDelayState::Loop", (int64)EDelayState::Loop },
				{ "EDelayState::Yoyo", (int64)EDelayState::Yoyo },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Loop.Name", "EDelayState::Loop" },
				{ "ModuleRelativePath", "Public/FCTweenInstance.h" },
				{ "None.Name", "EDelayState::None" },
				{ "Start.Name", "EDelayState::Start" },
				{ "Yoyo.Name", "EDelayState::Yoyo" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_FCTween,
				nullptr,
				"EDelayState",
				"EDelayState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
