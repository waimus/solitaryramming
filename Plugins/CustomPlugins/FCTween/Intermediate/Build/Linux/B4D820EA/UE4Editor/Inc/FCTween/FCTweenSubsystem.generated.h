// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FCTWEEN_FCTweenSubsystem_generated_h
#error "FCTweenSubsystem.generated.h already included, missing '#pragma once' in FCTweenSubsystem.h"
#endif
#define FCTWEEN_FCTweenSubsystem_generated_h

#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_SPARSE_DATA
#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_RPC_WRAPPERS
#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_RPC_WRAPPERS_NO_PURE_DECLS
#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFCTweenSubsystem(); \
	friend struct Z_Construct_UClass_UFCTweenSubsystem_Statics; \
public: \
	DECLARE_CLASS(UFCTweenSubsystem, UGameInstanceSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FCTween"), NO_API) \
	DECLARE_SERIALIZER(UFCTweenSubsystem)


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_INCLASS \
private: \
	static void StaticRegisterNativesUFCTweenSubsystem(); \
	friend struct Z_Construct_UClass_UFCTweenSubsystem_Statics; \
public: \
	DECLARE_CLASS(UFCTweenSubsystem, UGameInstanceSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FCTween"), NO_API) \
	DECLARE_SERIALIZER(UFCTweenSubsystem)


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFCTweenSubsystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFCTweenSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFCTweenSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFCTweenSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFCTweenSubsystem(UFCTweenSubsystem&&); \
	NO_API UFCTweenSubsystem(const UFCTweenSubsystem&); \
public:


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFCTweenSubsystem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFCTweenSubsystem(UFCTweenSubsystem&&); \
	NO_API UFCTweenSubsystem(const UFCTweenSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFCTweenSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFCTweenSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UFCTweenSubsystem)


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LastTickedFrame() { return STRUCT_OFFSET(UFCTweenSubsystem, LastTickedFrame); } \
	FORCEINLINE static uint32 __PPO__LastRealTimeSeconds() { return STRUCT_OFFSET(UFCTweenSubsystem, LastRealTimeSeconds); }


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_5_PROLOG
#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_PRIVATE_PROPERTY_OFFSET \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_SPARSE_DATA \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_RPC_WRAPPERS \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_INCLASS \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_PRIVATE_PROPERTY_OFFSET \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_SPARSE_DATA \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_RPC_WRAPPERS_NO_PURE_DECLS \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_INCLASS_NO_PURE_DECLS \
	vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h_8_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FCTWEEN_API UClass* StaticClass<class UFCTweenSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID vocaldysphoria_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
